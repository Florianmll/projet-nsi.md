## Projet Informatique 
Pour pouvoir lancer le programme, il faudra tout d'abord installer easygui al'aide de cette commande:
pip install easygui

Voicie mon projet de NSI:

```python
import random                  # On commence part importer les bibliothèque util au programme
from easygui import*
score = 0
 
questions = ["En quelle année Diego Maradona a-t-il marqué avec ses mains?\n->",
            "Dans quel sport pouvez-vous gagner la Coupe Davis?\n->",
            "Combien de minutes dure un match de rugby (Minute) ?\n->",        #On définit les question grace a une liste
            "Dans quel pays ont été les premiers Jeux Olympiques ?\n->",
            "Dans quel pays se trouve le circuit Interlagos F1?\n->"]
answers = ["1986","tennis","80","grece","brésil"]           # Et aussi les réponses
 
 
print("---------------------------------")
print("Vous exécutez le quiz de Florian")           #signer le quizz pour eviter le plagiat c'est important =)
print("---------------------------------\n")
 
question = random.sample(range(1,6),5)   #Faire en sorte qu les réponse soient demander aléatoirement
 
for num in question:
   if num == 1:
       Message11 = questions[0]
       title11 = "Mon Quiz"                        #Question 1
       question1 = (enterbox(Message11,title11))
       if question1.lower() == answers[0]:
           message1 = "Correct"
           title1 = "Mon quizz"                    #Résultat 1
           msgbox(message1, title1, "ok")
           score = score + 1
       else:
           message2 = "Incorrect"
           title2 = "Mon quizz"                    #Résultat 1
           msgbox(message2, title2, "ok")
   if num == 2:
       Message12 = questions[1]
       title12 = "Mon Quiz"                           #Question 2
       question2 = (enterbox(Message12,title12))
       if question2.lower() == answers[1]:
           message3 = "Correct"
           title3 = "Mon quizz"                        #Résultat 2
           msgbox(message3, title3, "ok")
           score = score + 1
       else:
            message4 = "Incorrect"
            title4 = "Mon quizz"                       #Résultat 2
            msgbox(message4, title4, "ok")
   if num == 3:
       Message13 = questions[2]
       title13 = "Mon Quiz"                            #Question 3
       question3 = (enterbox(Message13,title13))
       if question3.lower() == answers[2]:
           message5 = "Correct"                        #Résusltat 3            
           title5 = "Mon quizz"
           msgbox(message5, title5, "ok")
           score = score + 1
       else:
            message6 = "Incorrect"
            title6 = "Mon quizz"                       #Résultat 3
            msgbox(message6, title6, "ok")
   if num == 4:
       Message14 = questions[3]
       title14 = "Mon Quiz"                               # Question 4
       question4 = (enterbox(Message14,title14))
       if question4.lower() == answers[3]:
           message7 = "Correct"                           # Résultat 4
           title7 = "Mon quizz"
           msgbox(message7, title7, "ok")
           score = score + 1
       else:
            message7 = "Incorrect"
            title7 = "Mon quizz"                          # Résultat 4
            msgbox(message7, title7, "ok")
   if num == 5:
       Message15 = questions[4]
       title15 = "Mon Quiz"                                #Question 5
       question5 = (enterbox(Message15,title15))
       if question5.lower() == answers[4]:
           message8 = "Correct"                            #Résultat 5
           title8 = "Mon quizz"
           msgbox(message8, title8, "ok")
           score = score + 1
       else:
            message9 = "Incorrect"
            title9 = "Mon quizz"                           #Résultat 5
            msgbox(message9, title9, "ok")
 
message10 = "Votre score final est" ,score, "sur 5"
title10 = "Mon Quizz"                                       # Sore Final
msgbox(message10, title10, "Cool")
 
 
input("Appuyer sur entrer pour quitter")

```

C'est un quizz avec 5 questions qui sont posées aléatoirement, les bonnes
et les mauvaises sont comtabilisées avec un socre final.

## Vidéos Présentation:

Voici le lien de la vidéo montrant le programme en action, créer avec OBS et VLC Média Player:
https://youtu.be/o30dBzBQRKE